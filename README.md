# How to run the code

A definite automaton should be built manually:

    BasicDFASystem bs = new BasicDFASystem(false); # the false here means: reverse = false, which decide to build a origin system or reversed system
    bs.addBasicState(name, isInitial, isSecret);
    ...
    bs.addMap(start, end, signal, isObservable);
    ...
    bs.addSignal(signal_01, signal_02, ...);
    bs.updateMaps();

To verify centralized opacity:
    
    bs.verifyCSO(bs);
    bs.verifyTrellisBasedISO(bs)
    bs.verifyUnknownInitialGRBasedISO(bs);
    bs.verifyKnownInitialGRBasedISO(bs);
    
To verify decentralised opacity:
    
    # A joint estimator should be build first
    ArrayList<String> E_01 = new ArrayList<>(Arrays.asList(signal_01, signal_02, ...)); # the signals here represent observable signals for this intruder
    ...
    ArrayList<ArrayList<String>> Es = new ArrayList<ArrayList<String>>(Arrays.asList(E_01, E_02, E_03, ...));
    GRBasedJointEstimators gbje = new GRBasedJointEstimators(bs, Es);
    
    # Verification
    gbje.verifyJointISO();
    gbje.verifyJointCSO();
